import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:talkout_marvel/blocs/characters_bloc.dart';
import 'package:talkout_marvel/blocs/characters_event.dart';
import 'package:talkout_marvel/blocs/characters_state.dart';
import 'package:talkout_marvel/data/character.dart';
import 'package:talkout_marvel/services/marvel_api_service.dart';

class MockApiService extends Mock implements MarvelApiService {}

void main() {
  CharactersBloc charactersBloc;
  MockApiService mockApiService;

  setUp(() {
    mockApiService = MockApiService();
    charactersBloc = CharactersBloc(mockApiService);
  });

  tearDown(() {
    charactersBloc?.close();
  });

  group('bloc states', ()
  {
    test('api error results in failed state', () {
      when(mockApiService.getCharacters(any, any)).thenAnswer((_) async => ApiResult.error("error"));

      expectLater(
        charactersBloc,
        emits(FailedFetchingState("error", 0))
      );

      charactersBloc.add(FetchCharactersEvent(0));
    });

    test('api success results in fetched state', () {
      when(mockApiService.getCharacters(any, any)).thenAnswer((_) async => ApiResult.successful(CharacterData(100, 0, 20, [])));

      expectLater(
          charactersBloc,
          emits(CharactersFetchedState([], 0))
      );

      charactersBloc.add(FetchCharactersEvent(0));
    });

    test('correctly emits last page', () {
      when(mockApiService.getCharacters(any, any)).thenAnswer((_) async => ApiResult.successful(CharacterData(100, 80, 20, [])));

      expectLater(
          charactersBloc,
          emits(CharactersFetchedState([], 5, isLastPage: true))
      );

      charactersBloc.add(FetchCharactersEvent(5));
    });
  });
}