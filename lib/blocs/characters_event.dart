import 'package:equatable/equatable.dart';

abstract class CharactersEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class FetchCharactersEvent extends CharactersEvent {
  final int pageKey;

  FetchCharactersEvent(this.pageKey);

  @override
  List<Object> get props => [pageKey];
}