import 'package:equatable/equatable.dart';
import 'package:talkout_marvel/data/character.dart';

abstract class CharactersState extends Equatable {
  @override
  List<Object> get props => [];
}

class InitialState extends CharactersState {}

class CharactersFetchedState extends CharactersState {
  final List<Character> data;
  final bool isLastPage;
  final int pageKey;

  CharactersFetchedState(this.data, this.pageKey, {this.isLastPage = false});

  @override
  List<Object> get props => [data, isLastPage, pageKey];
}

class FailedFetchingState extends CharactersState {
  final String message;
  final int retries;

  FailedFetchingState(this.message, this.retries);

  @override
  List<Object> get props => [message, retries];
}