import 'package:equatable/equatable.dart';
import 'package:talkout_marvel/data/comic.dart';

class ComicState extends Equatable {
  final Comic comic;

  ComicState(this.comic);

  @override
  List<Object> get props => [comic];
}