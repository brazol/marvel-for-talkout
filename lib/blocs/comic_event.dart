import 'package:equatable/equatable.dart';
import 'package:talkout_marvel/data/comic.dart';

abstract class ComicEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class FetchComicImageEvent extends ComicEvent {
  final Comic comic;

  FetchComicImageEvent(this.comic);

  @override
  List<Object> get props => [comic];
}