import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:talkout_marvel/blocs/comic_event.dart';
import 'package:talkout_marvel/blocs/comic_state.dart';
import 'package:talkout_marvel/data/comic.dart';
import 'package:talkout_marvel/services/marvel_api_service.dart';


class ComicBloc extends Bloc<ComicEvent, ComicState> {
  MarvelApiService _marvelApiService;

  ComicBloc(this._marvelApiService, Comic comic) : super(ComicState(comic));

  @override
  Stream<ComicState> mapEventToState(ComicEvent event) async* {
    if(event is FetchComicImageEvent) {
      await _marvelApiService.fillComicThumbnail(event.comic);
      yield ComicState(event.comic);
    }
  }

}