import 'package:dio/dio.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:talkout_marvel/blocs/characters_state.dart';
import 'package:talkout_marvel/services/marvel_api_service.dart';

import 'characters_event.dart';

class CharactersBloc extends Bloc<CharactersEvent, CharactersState> {
  CancelToken _cancellationToken;
  MarvelApiService _marvelApiService;

  CharactersBloc(this._marvelApiService) : super(InitialState());

  @override
  Stream<CharactersState> mapEventToState(CharactersEvent event) async* {
    if(event is FetchCharactersEvent) {
      _cancellationToken = CancelToken();
      var result = await _marvelApiService.getCharacters(_cancellationToken, event.pageKey);

      if(result.isSuccessful)
        yield CharactersFetchedState(result.data.results, event.pageKey, isLastPage: result.data.isLastPage);
      else {
        var currentState = state;
        yield FailedFetchingState(result.errorMessage, (currentState is FailedFetchingState) ? currentState.retries + 1 : 0);
      }
    }
  }

  @override
  Future<void> close() {
    _cancellationToken?.cancel();
    return super.close();
  }
}