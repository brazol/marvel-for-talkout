import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:talkout_marvel/blocs/characters_bloc.dart';
import 'package:talkout_marvel/screens/character_details_screen.dart';
import 'package:talkout_marvel/screens/characters_list_screen.dart';
import 'package:talkout_marvel/services/marvel_api_service.dart';

void main() {
  runApp(MarvelApp());
}

class MarvelApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Marvel characters for TalkOut',
      theme: ThemeData(
        primarySwatch: Colors.red,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      onGenerateRoute: (settings) {
        Widget screen;

        switch(settings.name) {
          case CharacterDetailsScreen.routeName:
            screen = CharacterDetailsScreen(settings.arguments);
            break;
          default:
            throw Exception("unhandled root: ${settings.name}");
        }

        return PageRouteBuilder(
          settings: settings,
          transitionDuration: Duration(milliseconds: 400),
          transitionsBuilder: (_, Animation<double> animation, __, Widget child) {
            return new SlideTransition(
                position: new Tween<Offset>(
                  begin: const Offset(1.0, 0.0),
                  end: Offset.zero,
                ).animate(animation),
                child: child);
          },
          pageBuilder: (_, __, ___) => screen,
        );
      },
      home: Scaffold(
          appBar: AppBar(title: Text("Marvel characters"),),
          body: SafeArea(
              child: BlocProvider(
                  create: (context) => CharactersBloc(MarvelApiService.instance),
                  child: CharactersListScreen()))),
    );
  }
}
