import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:talkout_marvel/blocs/characters_bloc.dart';
import 'package:talkout_marvel/blocs/characters_event.dart';
import 'package:talkout_marvel/blocs/characters_state.dart';
import 'package:talkout_marvel/data/character.dart';
import 'package:talkout_marvel/widgets/character_list_item.dart';

class CharactersListScreen extends StatefulWidget {
  @override
  _CharactersListScreenState createState() => _CharactersListScreenState();
}

class _CharactersListScreenState extends State<CharactersListScreen> {
  final PagingController<int, Character> _pagingController = PagingController(firstPageKey: 0);
  CharactersBloc _charactersBloc;

  @override
  void initState() {
    super.initState();

    _charactersBloc = BlocProvider.of<CharactersBloc>(context);

    _pagingController.addPageRequestListener((pageKey) {
      _charactersBloc.add(FetchCharactersEvent(pageKey));
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<CharactersBloc, CharactersState>(
      listener: (_, state) {
        if (state is CharactersFetchedState) {
            if (state.isLastPage){
              _pagingController.appendLastPage(state.data);
            } else {
              _pagingController.appendPage(state.data, state.pageKey + 1);
            }
        } else if(state is FailedFetchingState) {
          Alert(context: context, type: AlertType.warning, title: "Ooops", desc: state.message, buttons: [
            DialogButton(
                child: Text("RETRY", style: Theme.of(context).textTheme.headline6.apply(color: Colors.white),),
                onPressed: () {
                  _charactersBloc.add(FetchCharactersEvent(0));
                  Navigator.pop(context);
                })
          ]).show();
        }
      },
      child: PagedListView.separated(
        pagingController: _pagingController,
        padding: const EdgeInsets.all(8),
        separatorBuilder: (context, index) => const SizedBox(
          height: 16,
          child: const Divider(),
        ),
        builderDelegate: PagedChildBuilderDelegate<Character>(
          itemBuilder: (context, item, index) => CharacterListItem(character: item),
        ),
      ),
    );
  }
}
