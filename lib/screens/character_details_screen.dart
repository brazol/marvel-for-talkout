import 'package:flutter/material.dart';
import 'package:talkout_marvel/data/character.dart';
import 'package:talkout_marvel/widgets/character_comics_card.dart';
import 'package:talkout_marvel/widgets/character_description_card.dart';

class CharacterDetailsScreen extends StatefulWidget {
  static const String routeName = "/character";

  final Character character;

  CharacterDetailsScreen(this.character);

  @override
  _CharacterDetailsScreenState createState() => _CharacterDetailsScreenState();
}

class _CharacterDetailsScreenState extends State<CharacterDetailsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              expandedHeight: 200.0,
              floating: false,
              pinned: true,
              flexibleSpace: FlexibleSpaceBar(
                  centerTitle: true,
                  title: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(widget.character.name),
                  ),
                  background: Image.network(widget.character.thumbnail.url,
                      fit: BoxFit.cover,
                      color: Colors.black.withOpacity(0.2),
                      colorBlendMode: BlendMode.darken)),
            ),
          ];
        },
        body: SafeArea(
          top: false,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child:  Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                CharacterDescriptionCard(character: widget.character),
                SizedBox(height: 15,),
                Expanded(child: CharacterComicsCard(character: widget.character)),
              ],
            ),
          ),
        ),
      ),
    );
  }
}