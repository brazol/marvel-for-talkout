import 'dart:async';
import 'package:dio/dio.dart';
import 'dart:convert';
import 'package:crypto/crypto.dart';
import 'package:talkout_marvel/data/character.dart';
import 'package:talkout_marvel/data/comic.dart';
import 'package:talkout_marvel/data/thumbnail.dart';

class ApiResult<T> {
  final T data;
  final bool isSuccessful;
  final String errorMessage;

  ApiResult(this.data, this.isSuccessful, this.errorMessage);

  factory ApiResult.successful(T data) => ApiResult(data, true, null);
  factory ApiResult.error(String errorMessage) => ApiResult(null, false, errorMessage);
}

class MarvelApiService {
  //TODO that's not a secure way to store keys at all, in production app I would use something like Firebase Remote Config
  static const _privateApiKey = "2e39bd11aa7a983ba211740a9e3934d96d0ed9da";
  static const _publicApiKey = "ec1fe12069f97cab14ba48c3be1123b0";
  static const _pageSize = 20;

  final String _baseEndpoint;
  final String _charactersEndpoint;
  final String _comicsEndpoint;

  MarvelApiService._private(this._baseEndpoint)
      : this._charactersEndpoint = "${_baseEndpoint}v1/public/characters",
        this._comicsEndpoint = "${_baseEndpoint}v1/public/comics/";

  static final MarvelApiService instance = MarvelApiService._private("https://gateway.marvel.com/");

  Future<ApiResult<CharacterData>> getCharacters(CancelToken cancelToken, int pageKey) async {
    final Map<String, dynamic> queryParams = {
      "limit": _pageSize,
      "offset": _pageSize * pageKey
    };

    try {
      final response = await Dio().get(_charactersEndpoint, queryParameters: queryParams..addAll(_getAuthQueryParams()), cancelToken: cancelToken);

      if (response.statusCode == 200) {
        var data = response.data["data"];
        var result = CharacterData.fromJson(data);

        return ApiResult.successful(result);
      } else {
        return ApiResult.error("Something went wrong, please try again.");
      }
    } on DioError catch(error) {
      switch(error.type) {
        case DioErrorType.DEFAULT:
          return ApiResult.error("App could not connect to server, please check your internet connection.");
        default:
          return ApiResult.error("App could not connect to server, try again later.");
      }
    }
  }

  Future<Comic> fillComicThumbnail(Comic comic) async {
    try {
      final response = await Dio().get("$_comicsEndpoint${comic.id}", queryParameters: _getAuthQueryParams());

      if (response.statusCode == 200) {
        var resultsList = response.data["data"]["results"] as List;
        var result = resultsList.first["thumbnail"];

        comic.thumbnail = MarvelApiThumbnail.fromJson(result);
      }
    } catch(e) {
      //Log error
    }

    return comic;
  }

  Map<String, dynamic> _getAuthQueryParams() {
    var timestamp = DateTime.now().millisecondsSinceEpoch;
    var hash = md5.convert(utf8.encode("$timestamp$_privateApiKey$_publicApiKey")).toString();

    return {
      "ts": timestamp,
      "hash": hash,
      "apikey": _publicApiKey
    };
  }
}