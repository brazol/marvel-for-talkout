import 'package:flutter/material.dart';
import 'package:talkout_marvel/data/character.dart';

class CharacterDescriptionCard extends StatelessWidget {
  final Character character;

  const CharacterDescriptionCard({
    Key key,
    @required this.character,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Padding(
        padding: const EdgeInsets.all(14.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Description", style: Theme.of(context).textTheme.headline6),
            const SizedBox(height: 10,),
            _getDescription(character.description)
          ],
        ),
      ),
    );
  }

  Widget _getDescription(String description) {
    if(description.isEmpty)
      return Center(child: Text("empty", style: TextStyle(fontStyle: FontStyle.italic)));

    return Text(character.description);
  }
}
