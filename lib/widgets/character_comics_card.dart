import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:talkout_marvel/blocs/comic_bloc.dart';
import 'package:talkout_marvel/blocs/comic_event.dart';
import 'package:talkout_marvel/data/character.dart';
import 'package:talkout_marvel/services/marvel_api_service.dart';
import 'package:talkout_marvel/widgets/comic_list_item.dart';

class CharacterComicsCard extends StatelessWidget {
  final Character character;

  const CharacterComicsCard({
    Key key,
    @required this.character,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Padding(
        padding: const EdgeInsets.all(14.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Comics", style: Theme.of(context).textTheme.headline6),
            Expanded(
              child: ListView.separated(
                  shrinkWrap: true,
                  itemCount: character.comicData.items.length,
                  itemBuilder: (context, index) {
                    var comic = character.comicData.items[index];

                    return BlocProvider(
                        create: (context) => ComicBloc(MarvelApiService.instance, comic)..add(FetchComicImageEvent(comic)),
                      child: ComicListItem(),
                    );
                  },
                separatorBuilder: (context, index)  => const SizedBox(
                  height: 16,
                  child: const Divider(),
                )),
            )
          ],
        ),
      ),
    );
  }
}
