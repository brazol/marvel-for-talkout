import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:talkout_marvel/blocs/comic_bloc.dart';
import 'package:talkout_marvel/blocs/comic_state.dart';

class ComicListItem extends StatefulWidget {
  const ComicListItem({Key key}) : super(key: key);

  @override
  _ComicListItemState createState() => _ComicListItemState();
}

class _ComicListItemState extends State<ComicListItem> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ComicBloc, ComicState>(builder: (context, state) {
      var thumbnailUrl = state.comic.thumbnail?.url;

      return ListTile(
        leading: Container(
              width: 50,
              height: 50,
              child: thumbnailUrl != null ? CachedNetworkImage(imageUrl: thumbnailUrl) : null),
        title: Text(state.comic.name),
      );
    });
  }
}
