import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:talkout_marvel/data/character.dart';
import 'package:talkout_marvel/screens/character_details_screen.dart';

class CharacterListItem extends StatelessWidget {
  final Character character;

  const CharacterListItem({
    Key key,
    @required this.character,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Container(
          width: 50,
          height: 50,
          decoration: new BoxDecoration(
              shape: BoxShape.circle,
              image: new DecorationImage(fit: BoxFit.cover, image: CachedNetworkImageProvider(character.thumbnail.url)))),
      title: Text(character.name),
      onTap: () async {
        await Navigator.pushNamed(context, CharacterDetailsScreen.routeName, arguments: character);
      },
    );
  }
}
