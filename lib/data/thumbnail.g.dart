// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'thumbnail.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MarvelApiThumbnail _$MarvelApiThumbnailFromJson(Map<String, dynamic> json) {
  return MarvelApiThumbnail(
    json['path'] as String,
    json['extension'] as String,
  );
}

Map<String, dynamic> _$MarvelApiThumbnailToJson(MarvelApiThumbnail instance) =>
    <String, dynamic>{
      'path': instance.path,
      'extension': instance.extension,
    };
