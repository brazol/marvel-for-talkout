// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'comic.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ComicData _$ComicDataFromJson(Map<String, dynamic> json) {
  return ComicData(
    (json['items'] as List)
        ?.map(
            (e) => e == null ? null : Comic.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$ComicDataToJson(ComicData instance) => <String, dynamic>{
      'items': instance.items,
    };

Comic _$ComicFromJson(Map<String, dynamic> json) {
  return Comic(
    Comic._idFromResourceUri(json['resourceURI'] as String),
    json['name'] as String,
    json['thumbnail'] == null
        ? null
        : MarvelApiThumbnail.fromJson(
            json['thumbnail'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$ComicToJson(Comic instance) => <String, dynamic>{
      'resourceURI': instance.id,
      'name': instance.name,
      'thumbnail': instance.thumbnail,
    };
