import 'package:json_annotation/json_annotation.dart';

part 'thumbnail.g.dart';

@JsonSerializable()
class MarvelApiThumbnail {
  final String path;
  final String extension;

  String get url => "$path.$extension";

  MarvelApiThumbnail(this.path, this.extension);

  factory MarvelApiThumbnail.fromJson(Map<String, dynamic> json) => _$MarvelApiThumbnailFromJson(json);
  Map<String, dynamic> toJson() => _$MarvelApiThumbnailToJson(this);
}