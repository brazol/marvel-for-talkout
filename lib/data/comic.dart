import 'package:json_annotation/json_annotation.dart';
import 'package:talkout_marvel/data/thumbnail.dart';

part 'comic.g.dart';

@JsonSerializable()
class ComicData {
  final List<Comic> items;

  ComicData(this.items);

  factory ComicData.fromJson(Map<String, dynamic> json) => _$ComicDataFromJson(json);
  Map<String, dynamic> toJson() => _$ComicDataToJson(this);
}

@JsonSerializable()
class Comic {
  @JsonKey(name: "resourceURI",fromJson: _idFromResourceUri)
  final int id;
  final String name;
  MarvelApiThumbnail thumbnail;

  Comic(this.id, this.name, this.thumbnail);

  factory Comic.fromJson(Map<String, dynamic> json) => _$ComicFromJson(json);
  Map<String, dynamic> toJson() => _$ComicToJson(this);

  static int _idFromResourceUri(String json) {
    if(json == null || json.isEmpty)
      return -1;

    return int.parse(json.split("/").last);
  }
}