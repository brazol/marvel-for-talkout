
import 'package:json_annotation/json_annotation.dart';
import 'package:talkout_marvel/data/comic.dart';
import 'package:talkout_marvel/data/thumbnail.dart';

part 'character.g.dart';

@JsonSerializable()
class CharacterData {
  final int total;
  final int offset;
  final int count;

  final List<Character> results;

  bool get isLastPage => offset + count >= total;

  CharacterData(this.total, this.offset, this.count, this.results);

  factory CharacterData.fromJson(Map<String, dynamic> json) => _$CharacterDataFromJson(json);
  Map<String, dynamic> toJson() => _$CharacterDataToJson(this);
}

@JsonSerializable()
class Character {
  final int id;
  final String name;
  final String description;
  final MarvelApiThumbnail thumbnail;

  @JsonKey(name: "comics")
  final ComicData comicData;

  Character(this.id, this.name, this.description, this.thumbnail, this.comicData);

  factory Character.fromJson(Map<String, dynamic> json) => _$CharacterFromJson(json);
  Map<String, dynamic> toJson() => _$CharacterToJson(this);
}