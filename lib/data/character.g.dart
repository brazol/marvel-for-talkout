// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'character.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CharacterData _$CharacterDataFromJson(Map<String, dynamic> json) {
  return CharacterData(
    json['total'] as int,
    json['offset'] as int,
    json['count'] as int,
    (json['results'] as List)
        ?.map((e) =>
            e == null ? null : Character.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$CharacterDataToJson(CharacterData instance) =>
    <String, dynamic>{
      'total': instance.total,
      'offset': instance.offset,
      'count': instance.count,
      'results': instance.results,
    };

Character _$CharacterFromJson(Map<String, dynamic> json) {
  return Character(
    json['id'] as int,
    json['name'] as String,
    json['description'] as String,
    json['thumbnail'] == null
        ? null
        : MarvelApiThumbnail.fromJson(
            json['thumbnail'] as Map<String, dynamic>),
    json['comics'] == null
        ? null
        : ComicData.fromJson(json['comics'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$CharacterToJson(Character instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'description': instance.description,
      'thumbnail': instance.thumbnail,
      'comics': instance.comicData,
    };
